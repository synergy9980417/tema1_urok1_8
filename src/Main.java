//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.

import java.util.Scanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Main {
    public static void main(String[] args) throws IOException {

       System.out.println("!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
   Scanner scanner = new Scanner(System.in);
    int x = scanner.nextInt();
    int y = scanner.nextInt();
       System.out.println("max is "+(x>y?x:y));

       System.out.println("!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
       System.out.println((x>=y?"икс >= игрик":"икс < игрик"));

       System.out.println("!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
       x=scanner.nextInt();
       y=scanner.nextInt();
       int z=scanner.nextInt();
       int max = ((x>=y) && (x>=z)) ? x : ((y>=z) && (y>=x) ? y : z);
       System.out.println(max);

        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        //у меня не стал работать этот api поэтому взял другой аналогичный
        //https://breakingbadapi.com/api/quote/random
        String page = downloadWebPage("https://favqs.com/api/qotd");
        System.out.println(page);
        int qStart=page.lastIndexOf("\",\"body\":\"");
        String quote= page.substring(qStart+10,page.length()-3);
        System.out.println("Цитата: "+quote);

        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        if (quote.length()>50){
            System.out.println(quote.substring(0,50)+"...");
        } else {
            System.out.println(quote);
        }

        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println((quote.length()>50) ? quote.substring(0,50)+"..." : quote);


        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        int aStart=page.lastIndexOf("\"author_permalink\"");
        String authorQ=page.substring(aStart+20,qStart);
        System.out.println("Автор: ("+authorQ+")");

        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 8 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println(!authorQ.equals("Wallter White")?"Здесь нет Wallter White, чего и следовало ожидать":"неужели и здесь есть цитата Wallter White, невероятно");

    }

    private static String downloadWebPage(String url) throws IOException{
        StringBuilder result = new StringBuilder();
        String line;
        URLConnection urlConnection = new URL(url).openConnection();
        try (InputStream is = urlConnection.getInputStream();
        BufferedReader br= new BufferedReader(new InputStreamReader(is))){
            while ((line=br.readLine())!=null){
                result.append(line);
            }
        }
        return result.toString();
    }




}
